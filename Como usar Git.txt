
===============================================================================================================

COMO USAR O GIT - SALVANDO NO REPOSITÓRIO LOCAL

1. Crie o repositório .git com o git init
2. Coloque os arquivos na pasta com o repositório .git
3. Adicione os arquivos na área de espera usando o git add (com isso adicionamos eles para controle de versão)
4. Salve os arquivos no repositório local dando git commit -m "mensagem (opcional)"

(Se você quiser que o Git ignore arquivos, crie o .gitignore e coloque o nome do arquivos(s) lá. 
Daí o Git não lista mais ele nem nada)

----------------------------------------------------------------------------------------------------------------

COMO USAR O GIT - VENDO ALTERAÇÕES


ARQUIVOS DA PASTA LOCAL

1. Acesse a pasta que tem o repositório .git
2. git diff (mostra diferenças nos arquivos da pasta)


ARQUIVOS DA ÁREA DE ESPERA

1.Depois de dar o git add, use git diff --staged.


HISTÓRICO DE MUDANÇAS NO COMMIT

a) Use git log 

Ele mostra quem fez a mudança, a data e o que mudou, mas não fala os arquivos mudados. 

b) Use o git log -p

Ele mostra os commits em ordem do mais novo para o mais velho, quem fez, a data, e o que mudou em cada arquivo.

c) Use o git log -p -n

Mesma coisa do git log -p, mas só pega n commits.


DICA: Para ver de forma detalhada, porém simples, use uma interface gráfica para o Git, disponível no site do mesmo.
Eu instalei o gitk.

Logo, usarei o comando gitk para ver as mudanças.


VENDO A CHAVE DE COMMITS E SUAS MENSAGENS

1.Use o comando git log --pretty=oneline

------------------------------------------------------------------------------------------------------------------


COMO USAR O GIT - DESFAZENDO MUDANÇAS


EDITANDO UM COMMIT

1.Caso queira juntar alterações em um arquivo que foi salvo em um commit anterior, primeiramente, use o git add no arquivo.
2.Use o git commit --amend -m "(mensagem)".

Com isso, o arquivo do commit vai ser editado e o commit fica com uma mensagem nova.


DESFAZENDO MODIFICAÇÕES EM ARQUIVO NA PASTA DE TRABALHO

1.Use o comando git checkout -- (nome do arquivo).
Ele descarta modificações feitas no arquivo na pasta de trabalho. O arquivo volta para o estado do último commit.


PARA PEGAR ARQUIVO NO STATUS ORIGINAL DO ÚLTIMO COMMIT

git reset -- (nome do arquivo) (Copia do último commit para a STAGING AREA)
git checkout -- (nome do arquivo) (Copia da STAGING AREA para o WORKING DIRECTORY)


REMOVENDO ARQUIVO DO REPOSITÓRIO LOCAL

1.Use o comando git rm (nome do arquivo).


REMOVENDO ARQUIVO DA ÁREA DE ESPERA

1.Use o comando git rm --cached (nome do arquivo).
-----------------------------------------------------------------------------------------------------------------------


COMO USAR O GIT - TAGS

TAGS são etiquetas que você pode colocar nos commits para marcá-los e, assim, marcar as versões diferentes dos arquivos.
Assim, fica mais fácil reverter o sistema para uma tag específica, uma versão específica


LISTANDO AS TAGS EXISTENTES

1.Use o comando git tag


COLOCANDO TAG NO COMMIT ATUAL

1.Use o comando git tag -a (nome da tag) -m "(mensagem)".

Assim, você cria uma tag anotada. Dá pra saber quem e em que horário criaram aquela tag.


COLOCANDO TAG EM COMMIT ANTIGO

1.Use o comando git tag -a (nome da tag) (chave do commit) -m "(mensagem)".


VENDO O CONTEÚDO DA TAG
1.Para ver mais detalhes de uma tag, use o comando git show (tag).


TROCANDO OS ARQUIVOS PELOS ARQUIVOS DA TAG
1.Para trocar os arquivos atuais para os arquivos da Tag, use o comando git checkout (nome da tag).


APAGANDO A TAG

1.Use o comando git checkout master.
2.Use o comando git tag para ver as tags.
3.Use o comando git tag -d (nome da tag) para apagar a tag.
---------------------------------------------------------------------------------------------------------------------


COMO USAR O GIT - BRANCHES


Branches são ramificações de uma coisa.

Com branches, podemos criar ramificações de uma pasta, ou seja, cópias dela.
Isso pode ser útil quando quisermos fazer testes ou modificações sem arriscar os arquivos originais, pois o que ocorre em um branch
não afeta o outro branch.


CRIANDO BRANCH

1.Use o comando git branch (nome).


TROCANDO PARA A PASTA BRANCH

1.Use o comando git checkout (branch).



TRAZENDO ALTERAÇÕES DE UM BRANCH PARA O BRANCH MASTER

Para isso, faremos um merge (mesclar).

OBS.: Esteja no branch que você quer que receba as modificações

1.Use o comando git merge (nome do branch).

Isso vai trazer as modificações do branch inserido para o branch atual.


APAGANDO BRANCH

1.Use o comando git branch -d (nome do branch).


LISTANDO TODOS OS BRANCHES

1.Use o comando git branch.
-------------------------------------------------------------------------------------------------------------------


COMO USAR O GIT - GITHUB/GITLAB


CONFIGURANDO SSH

Com a chave SSH inserida no perfil, podemos acesar o GitHub/GitLab a partir do computador cuja chave SSH foi inserida.

1.Use o comando ssh-keygen.
2.Entre o arquivo id_rsa.pub
3.Copie a chave
4.Cadastre ela no GitHub/GitLab.


CLONAR REPOSITÓRIO


Clonar repositório significa copiar ele para a sua máquina.

1.Vá no projeto no site.
2.Copie o endereço SSH/HTTPS.
3.Use o comando git clone (endereço copiado).


Se você quiser mudar o nome do repositório (localmente), faça:

git clone (endereço copiado) (nome).


ADICIONANDO ARQUIVO NO REPOSITÓRIO ONLINE

1.Primeramente, faça o commit.
2.Use o comando git push origin master (isso no branch master do repositório clonado, que contém o .git do GitHub/GitLab).

OBS.: Geralmente o servidor do repositório, clonado, é "origin".
Para saber o nome do servidor, use git remote.

OBS.: O Git faz o merge automático dos arquivos na pasta local.


PEGANDO ARQUIVO NO REPOSITÓRIO ONLINE.

1.Vá para o repositório clonado
2.Use o comando git pull origin master (isso no branch master do repositório clonado, que contém o .git do GitHub/GitLab).

OBS.: Geralmente o servidor do repositório, clonado, é "origin".
Para saber o nome do servidor, use git remote.


PEGANDO ARQUIVO NO REPOSITÓRIO ONLINE PARA O BRANCH

1.Use o comando git fetch (nome do servidor) (nome do branch) 
Isso é bom para evitar o merge automático no master.


EXCLUINDO REPOSITÓRIO

1.Vá no repositório que você quer deletar.
2.Vá em Settings.

Lá vai ter uma opção para deletar o repositório.

No GitLab, vá em Advanced--->Expand-->Remove Project
No GitHub, clique em Delete Repository.

===================================================================================================


COMO USAR GIT - COLABORANDO COM PROJETOS OPEN SOURCE


Quando se tem um projeto público, você abre espaço para que colaboradores vão ajudando com os arquivos.

Antes de mais nada, vamos entender o que é Fork.
Fork = você clonar aquele repositório para o seu perfil no GitHub/GitLab.



FAZENDO FORK DE UM REPOSITÓRIO

1.Vá na página do projeto que você quer colaborar.
2. Clique no botão Fork

Pronto, agora ele vai estar na sua lista de repositórios


COLABORANDO COM O PROJETO

1.Clone o projeto para a sua máquina, usando o git clone.
2.Faça as modificações.
3.Faça o git add e o git commit.
4.Faça o git push para o GitHub/GitLab.


Agora o repositório NA SUA CONTA foi modificado. Isso quer dizer que o original, do autor(a), não mudou nada.
Para modificar o repositório do autor(a), faça o PULL REQUEST.


FAZENDO PULL REQUEST/MERGE REQUEST


1.No repositório do projeto NA SUA CONTA, clique no botão Pull Request no GitHub (ou Merge Request no GitLab)
2.Siga as instruções e mande o request.

Agora que você enviou o request com as modificações, sua colaboração acabou. 

No request o autor(a)vai poder ver todas as mudanças que você fez.

Se o autor quiser que as suas mudanças sejam implementadas no projeto, ele aprova o requeste e ocorre um merge dos seus arquivos com os deles.
